#clones the repo
git clone https://github.com/denomparkour/shell-bot
#changes dir to shell-bot
cd shell-bot
#Installs python and build-essential
sudo apt install -y make python build-essential
#Runs npm install
npm install
#Starts the bot
node server
